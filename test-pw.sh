#!/bin/bash

#Choix du mot de passe à tester
echo -n "Mot de passe à tester : "
read -s password
echo

# Lister les utilisateurs
USERS=$(yunohost user list | grep username | awk '{print $2}')
COMPTE=0

# Tester le bind LDAP
for USER in $USERS
do
if ldapwhoami -D uid=$USER,ou=users,dc=yunohost,dc=org -w $password > /dev/null 2>&1; then
    ((COMPTE++))
fi
done

#Renvoyer le résultat
echo $COMPTE "comptes trouvés"
